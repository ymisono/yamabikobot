﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace YamabikoBot.Dialogs
{
    [LuisModel("871d4b8c-6846-48bf-a65d-9bedbc96f161", "9d2a2bbd525c4196b6781774e9f3b6cf")]
    [Serializable]
    public class TestDialog : LuisDialog<object>
    {
        [LuisIntent("ask_location")]
        public async Task AskLocation(IDialogContext context, LuisResult result)
        {
            string parameter = "";
            EntityRecommendation title;
            if (result.TryFindEntity("encyclopedia", out title))
            {
                parameter = title.Entity;
            }
            else if (result.TryFindEntity("shop", out title))
            {
                parameter = title.Entity;
            }

            await context.PostAsync($"{parameter} is here.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("greeting")]
        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            await context.PostAsync($"{result.Query}");
            context.Wait(MessageReceived);
        }

        [LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult result)
        {
            string message = $"Sorry I did not understand: " + string.Join(", ", result.Intents.Select(i => i.Intent));
            await context.PostAsync(message);
            context.Wait(MessageReceived);
        }
    }

}